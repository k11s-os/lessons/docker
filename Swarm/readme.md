# Docker Swarm Cluster

> https://labs.play-with-docker.com
> 
> https://training.play-with-docker.com/swarm-mode-intro/

## Install docker

```shell
$ yum install -y yum-utils
$ yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
$ yum install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin
$ systemctl enable docker --now
```


```shell
$ docker swarm init
$ docker node ls
$ docker swarm join
$ docker node ls
```

### Remove node from Cluster

Run this commands on node to be removed

```shell
$ docker swarm leave
$ docker node ls
```

Run this command on master node
```shell
$ docker node rm node2
```

### Add node to Existing Cluster

Run this command on master node

```shell
$ docker swarm join-token worker
```

## Play with Swarm Cluster

### Portainer

> https://docs.portainer.io/start/install/server/swarm/linux

```shell
$ curl -L https://downloads.portainer.io/ce2-16/portainer-agent-stack.yml -o portainer-agent-stack.yml
$ docker stack deploy -c portainer-agent-stack.yml portainer
```

In browser

>https://localhost:9443

```shell
$ docker stack deploy
$ docker stack ls
$ docker stack rm
```

### Cluster VIP via Pacemaker

```shell
$ ansible-playbook -i inventories/dev pacemaker_install.yml
$ systemctl stop firewalld --now
```

### Grafana Dashboards

* [Redis Dashboard](https://grafana.com/grafana/dashboards/763)
* [Go Dashboard](https://grafana.com/grafana/dashboards/13240)
* [Fluent-Bit Dashboard](https://github.com/fluent/fluent-bit-docs/tree/8172a24d278539a1420036a9434e9f56d987a040/monitoring/dashboard.json)
* [PGbouncer Dashboard 1](https://grafana.com/grafana/dashboards/11806)
* [PGbouncer Dashboard 2](https://grafana.com/grafana/dashboards/13353)

### Load Balancing

```shell
$ docker swarm init --advertise-addr <load balancer private IP>
$ sudo mkdir -p /data/loadbalancer
$ sudo vi /data/loadbalancer/default.conf
```

```text
server {
   listen 80;
   location / {
      proxy_pass http://backend;
   }
}
upstream backend {
   server <node0 private IP>:8080;
   server <node1 private IP>:8080;
}
```

```shell
$ docker service create --name loadbalancer --mount type=bind,source=/data/loadbalancer,target=/etc/nginx/conf.d --publish 80:80 nginx
```


### Sentry

For SENTRY_DSN goto sentry.io
The register and create project, select type GO and copy from code DNS
for example https://gfhdfghfdghdfg6b9918@o350042.ingest.sentry.io/45745679936
Then paste it to env file

### Redis Cache stack

Check logs of app, if err restart container
Check logs of pgbouncer exporter
Check prometheus targets for all available
